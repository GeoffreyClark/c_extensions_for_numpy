//Inspired by: http://www.scipy-lectures.org/advanced/interfacing_with_c/interfacing_with_c.html
//and: https://github.com/numpy/numpy/issues/5313

#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>

static PyObject* mean_std(PyObject* self, PyObject* args)
{
  int count = 0, i = 0;//, arrLen;
  float sum = 0;
  float mean = 0;
  float stdev = 0;
  int typenum = NPY_DOUBLE;
  PyArrayObject *in_array;

  //parse in single numpy array type objects
  if(!PyArg_ParseTuple(args, "O!", &PyArray_Type, &in_array))
    return NULL;

  //get number of dimensions of array:
  npy_intp num_dims = PyArray_NDIM(in_array);

  PyArray_Descr *descr = PyArray_DescrFromType(typenum);
  npy_intp dims[4];

  //as PyArray_AsCArray steals reference
  Py_INCREF(in_array);

  //convert numpy to c array
  double *arr;
  if(PyArray_AsCArray((PyObject **) &in_array, (void **) &arr, dims, num_dims, descr) < 0){
    PyErr_SetString(PyExc_TypeError, "error converting to c array");
    return NULL;
  }

  //calculate mean:
  i=0;
  for( ;i<dims[0];i++){
    count = count + 1;
    sum = sum + arr[i];
  }
  mean = sum/count;

  //calculate stdev:
  sum = 0;
  i = 0;
  for(;i<dims[0];i++){
    sum = sum + (arr[i] - mean)*(arr[i] - mean);
  }
  stdev = pow((float)sum/(count-1),0.5);//use the unbiased estimator (ie sample stedev)

  //free c-like array
  PyArray_Free((PyObject *) in_array, (void *) arr);

  //return the result:
  return Py_BuildValue("ff", mean, stdev);
}

static PyMethodDef functions[] = {
  //ml_name(python function name), ml_meth(c function name), ml_flags(METH_VARARGS,METH_NOARGS,METH_KEYWORDS), ml_doc(docstring explaining function - or just NULL)
  {"mean_std", mean_std, METH_VARARGS, "return mena and stdev of a np array"},
  {NULL, NULL, 0, NULL}//needs to be ended with this sentinal of NULL and 0 values
};

PyMODINIT_FUNC
initnp_ext_carr(void)
{
  //Py module name, method mapping table defined above, docstring of comments
  (void) Py_InitModule("np_ext_carr", functions);
  import_array();//NB! This must be called.
}
