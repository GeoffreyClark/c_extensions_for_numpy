from distutils.core import setup, Extension
import numpy

# define the extension module
module = Extension('np_ext_iter', sources=['np_ext_iter.c'],
                          include_dirs=[numpy.get_include()])

# run the setup
setup(name = 'np_ext_iter',
       version = '1.0',
       ext_modules=[module])

#####################################################################33
#try implementing the c extensions in a more c-like way.

module = Extension('np_ext_carr', sources=['np_ext_carr.c'],
                          include_dirs=[numpy.get_include()])

# run the setup
setup(name = 'np_ext_carr',
       version = '1.0',
       ext_modules=[module])
