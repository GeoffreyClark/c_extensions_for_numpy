//Inspired by: http://www.scipy-lectures.org/advanced/interfacing_with_c/interfacing_with_c.html

#include <Python.h>
#include <numpy/arrayobject.h>
#include <math.h>

static PyObject* mean_std(PyObject* self, PyObject* args)
{
  PyArrayObject *in_array;
  NpyIter *in_iter;
  NpyIter_IterNextFunc *in_iternext;
  double ** in_dataptr;
  int count = 0;
  float sum = 0;
  float mean;
  float stdev;
  char arrType;

  //parse in single numpy array type objects
  if(!PyArg_ParseTuple(args, "O!", &PyArray_Type, &in_array))
    return NULL;

  //create iterator:
  in_iter = NpyIter_New(in_array, NPY_ITER_READONLY, NPY_KEEPORDER, NPY_NO_CASTING, NULL);
  if(in_iter == NULL)
    goto fail;

  in_iternext = NpyIter_GetIterNext(in_iter, NULL);
  if(in_iternext == NULL){
    NpyIter_Deallocate(in_iter);
    goto fail;
  }//end if

  in_dataptr = (double **) NpyIter_GetDataPtrArray(in_iter);

  //Calculate the mean
  do{
    count = count + 1;
    sum = sum + **in_dataptr;
  }while(in_iternext(in_iter));
  mean = (float)sum/count;

  //re-create iterator:
  in_iter = NpyIter_New(in_array, NPY_ITER_READONLY, NPY_KEEPORDER, NPY_NO_CASTING, NULL);
  if(in_iter == NULL)
    goto fail;

  in_iternext = NpyIter_GetIterNext(in_iter, NULL);
  if(in_iternext == NULL){
    NpyIter_Deallocate(in_iter);
    goto fail;
  }//end if

  in_dataptr = (double **) NpyIter_GetDataPtrArray(in_iter);

  //calculate the standard deviation
  sum = 0;
  do{
    sum = sum + (**in_dataptr - mean)*(**in_dataptr - mean);
  }while(in_iternext(in_iter));
  stdev = pow((float)sum/(count-1),0.5);//use the unbiased estimator

  //clean up and return the result:
  NpyIter_Deallocate(in_iter);

  return Py_BuildValue("ff", mean, stdev);

  //error handeler
  fail:
    printf("There was an error in mean_std");
    Py_RETURN_NONE;
}

static PyMethodDef functions[] = {
  //ml_name(python function name), ml_meth(c function name), ml_flags(METH_VARARGS,METH_NOARGS,METH_KEYWORDS), ml_doc(docstring explaining function - or just NULL)
  {"mean_std", mean_std, METH_VARARGS, "return mena and stdev of a np array"},
  {NULL, NULL, 0, NULL}//needs to be ended with this sentinal of NULL and 0 values
};

PyMODINIT_FUNC
initnp_ext_iter(void)
{
  //Py module name, method mapping table defined above, docstring of comments
  (void) Py_InitModule("np_ext_iter", functions);
  import_array();//NB! This must be called.
}
