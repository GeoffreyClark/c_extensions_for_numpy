import numpy as np
import np_ext_iter
import np_ext_carr
import time

mu, sigma = 10, 2
size = 1000#when this gets bigger numpy does better:/
x = np.random.normal(mu, sigma,size).astype(np.float64)
#x = np.arange(size).astype(np.float64)

print "np_ext_iter:"
mean, stdev = np_ext_iter.mean_std(x)
print "Mean:", mean, 'Stdev:', stdev

print "np_ext_carr:"
mean, stdev = np_ext_carr.mean_std(x)
print "Mean:", mean, 'Stdev:', stdev

print "numpy:"
mean = x.mean()
stdev = x.std(ddof=1)#unbiased estimator (ie sample stdev)
print "Mean:", mean, 'Stdev:', stdev

#Timed tests:
n = 10000

print '\nWith np_ext_iter:'
start = time.time()
for i in xrange(n):
    mean, stdev = np_ext_iter.mean_std(x)
print 'Time taken: ', time.time() - start

print '\nWith np_ext_carr:'
start = time.time()
for i in xrange(n):
    mean, stdev = np_ext_carr.mean_std(x)
print 'Time taken: ', time.time() - start

#calculated with numpy
print '\nWith Numpy:'
start = time.time()
for i in xrange(n):
    mean = x.mean()
    stdev = x.std(ddof=1)#unbiased estimator (ie sample stdev)
print 'Time taken: ', time.time() - start
