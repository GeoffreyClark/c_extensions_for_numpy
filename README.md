# C Extensions for Numpy #

The goal was to see if I could get a performance improvement on calculating the mean and standard deviation of a Numpy array if I calculated them both simultaneously in c (the mean is required to calculate the standard deviation).

The end results was a large performance improvement for small arrays but worse performance for large arrays (presumably because NumPy seems to cache arrays).

This repo contains c files that compile back to Python Libraries that can perform these operations on NumPy arrays.

### What is this repository for? ###

* There are two simple NumPy extensions in C
* The first uses the NumPy c-api to iterate over the all the elements in the array (np_ext_iter)
* The second converts the NumPy array to a c-like array and handels that calcuations natively in c (np_ext_carr)
* The second approach is faster

### How do I get set up? ###

* Clone the repo
* Once inside the repo you can compile the c extensions by running the command: "python setup.py install"
* You can then run the performance test (NumPy versus the 2 extensions) with the command: "python test_performance.py"
